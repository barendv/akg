#!/bin/sh
echo "AKG: Automatic Key Generation -- friendlier way of generating RSA keys"

echo "[g]enerate, [e]ncrypt or [d]ecrypt?"
read EOD
if [ $EOD = "g" ]
then
	echo "Name of private key: "
	read PRKN
	echo "Name of public key: "
	read PUKN
	echo "Key strength: "
	read KS
	openssl genrsa -out $PRKN.pem $KS
	openssl rsa -in $PRKN.pem -out $PUKN.pem -outform PEM -pubout
fi
if [ $EOD = "e" ]
then
	echo "Public key: "
	read PUKN
	echo "File to encrypt: "
	read FTE
	echo "Output file: "
	read OF
	openssl rsautl -encrypt -inkey $PUKN.pem -pubin -in $FTE -out $OF.ssl
fi
if [ $EOD = "d" ]
then
	echo "Private key: "
	read PRKN
	echo "Encrypted file: "
	read FTD
	echo "Output file: "
	read OF
	openssl rsautl -decrypt -inkey $PRKN.pem -in $FTD -out $OF
fi
