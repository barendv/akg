# AKG #

AKG stands for Automatic Key Generator and takes the pain out of generating OpenSSL RSA keys. It's built on top of a [tutorial](http://is.gd/pupret) by R.I. Pienaar.

Run


```
#!shell
git clone https://bitbucket.org/barendv/akg.git

```